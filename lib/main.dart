import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';

void main() => runApp(new SciHubApp());

class SciHubApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SciHub',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: LinkPage(),
    );
  }
}

class CustomPopupMenu {
  CustomPopupMenu({this.title, this.description});

  String title;
  String description;
}

class LinkPage extends StatefulWidget {
  LinkPage({Key key}) : super(key: key);

  @override
  _LinkPageState createState() => _LinkPageState();
}

class _LinkPageState extends State<LinkPage> {
  static const String info =
      "Use your browser to find a research article's DOI, open the link with 'DOI to SciHub,' then download from a domain below.";
  static const List scihubsites = [
    'https://sci-hub.do/',
    'https://sci-hub.se/',
    'https://sci-hub.st/'
  ];
  static const List torscihubsites = [
    'https://sci-hub.do/',
    'https://sci-hub.se/',
    'https://sci-hub.st/',
    'https://scihub22266oqcxt.onion.link/',
    'https://scihub22266oqcxt.onion.to/',
    'https://scihub22266oqcxt.onion.cab/',
    'https://scihub22266oqcxt.onion.direct/',
  ];
  List _scihubsites = scihubsites;
  String _dataShared = '';
  bool dropdowndelta = false;

  String _selectedURL;
  String sharedURL;
  String _visibleDOI;
  List<DropdownMenuItem<String>> _dropDownMenuItems;

  @override
  void initState() {
    super.initState();
    getSharedText();
  }

  void overflowSelection(CustomPopupMenu selection) {
    switch (selection.description) {
      case 'domains':
        if (_scihubsites.length < 4) {
          _scihubsites = torscihubsites;
          newDomainList();
        } else {
          _scihubsites = scihubsites;
          newDomainList();
        }
        break;

      case 'about':
        _launchURL(context, 'https://sci-hub.do/about');
        break;
    }
  }

  List<DropdownMenuItem<String>> buildAndGetDropDownMenuItems(List websites) {
    List<DropdownMenuItem<String>> items = new List();
    for (String website in websites) {
      items.add(new DropdownMenuItem(
          value: website + _dataShared, child: new Text(website)));
    }
    return items;
  }

  List<CustomPopupMenu> choices = <CustomPopupMenu>[
    CustomPopupMenu(title: 'Toggle onion domains', description: 'domains'),
    CustomPopupMenu(title: 'What is SciHub?', description: 'about'),
  ];

  @override
  Widget build(BuildContext context) {
    double imagescale = (MediaQuery.of(context).size.height * 1.5) - 813.95;

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: new AppBar(
            title: const Text('DOI to SciHub'),
            backgroundColor: Theme.of(context).primaryColorDark,
            actions: <Widget>[
              PopupMenuButton<CustomPopupMenu>(
                onSelected: overflowSelection,
                itemBuilder: (BuildContext context) {
                  return choices.map((CustomPopupMenu choice) {
                    return PopupMenuItem<CustomPopupMenu>(
                      value: choice,
                      child: Text(choice.title),
                    );
                  }).toList();
                },
              )
            ]),
        body: new Container(
          child: new Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset('assets/logo.jpg', height: imagescale),
                Spacer(),
                new Text(info,
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.center),
                Spacer(),
                new SizedBox(
                  height: 1.0,
                  child: new Center(
                      child: new Container(
                          margin: new EdgeInsetsDirectional.only(
                              start: 1.0, end: 1.0),
                          height: 1.0,
                          color: Theme.of(context).primaryColorDark)),
                ),
                Spacer(),
                new FlatButton(
                  child: Text(_visibleDOI,
                      style: TextStyle(
                          fontSize: 15, fontWeight: FontWeight.normal),
                      textAlign: TextAlign.center),
                  onPressed: () => _launchURL(context, _dataShared),
                  splashColor: Theme.of(context).primaryColorDark,
                ),
                Spacer(),
                new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new DropdownButton(
                          value: _selectedURL,
                          items: _dropDownMenuItems,
                          onChanged: changedDropDownItem,
                          iconSize: 30,
                          elevation: 16,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w800,
                              letterSpacing: 0.7))
                    ]),
                Spacer(),
                new RaisedButton(
                  child: Text('Download',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w700)),
                  color: Theme.of(context).primaryColorDark,
                  elevation: 8.0,
                  splashColor: Theme.of(context).primaryColor,
                  onPressed: () => _launchURL(context, _selectedURL),
                ),
                Spacer(flex: 2),
              ],
            ),
          ),
        ));
  }

  getSharedText() async {
    const platform = const MethodChannel('app.channel.shared.data');
    sharedURL = await platform.invokeMethod("getURL");
    setState(() {
      _dataShared = sharedURL ?? '';
      _visibleDOI = sharedURL ?? '[NO DOI SELECTED]';
    });
    newDomainList();
  }

  void newDomainList() {
    setState(() {
      _dropDownMenuItems = buildAndGetDropDownMenuItems(_scihubsites);
      _selectedURL = _dropDownMenuItems[0].value;
    });
  }

  void changedDropDownItem(String selectedURL) {
    setState(() {
      dropdowndelta = true;
      _selectedURL = selectedURL;
    });
  }
}

void _launchURL(BuildContext context, String site) async {
  try {
    await launch(
      site,
      option: new CustomTabsOption(
        toolbarColor: Theme.of(context).primaryColorDark,
        enableDefaultShare: true,
        enableUrlBarHiding: true,
        showPageTitle: true,
        animation: new CustomTabsAnimation.slideIn(),
        extraCustomTabs: <String>[
          // https://play.google.com/store/apps/details?id=org.mozilla.firefox
          'org.mozilla.firefox',
          // https://play.google.com/store/apps/details?id=com.microsoft.emmx
          'com.microsoft.emmx',
        ],
      ),
    );
  } catch (e) {
    debugPrint(e.toString());
  }
}
